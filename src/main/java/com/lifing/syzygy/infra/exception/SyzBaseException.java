package com.lifing.syzygy.infra.exception;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;

import com.lifing.syzygy.infra.util.MessageSourceBundle;

public class SyzBaseException extends RuntimeException implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	// Primitive String definition
	protected String id;
	protected String[] ids;
	protected String msg;
	protected String message;
	protected StringBuilder sbldErrorMessage = new StringBuilder();
	// Object definitions
	protected Errors errors;
	protected Object[] args;

	// @Autowired
	// private MessageSourceBundle msgSourceBundle;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	/** Constructs a new BaseException with null as its detail message. */
	public SyzBaseException() {
		super();
	}

	public SyzBaseException(String message) {
		super(message);
	}

	public SyzBaseException(String id, String message) {
		this.id = id;
		this.message = message;
	}

	/**
	 * Constructs a new BaseException with the specified message id and Object
	 * arguments.
	 */
	public SyzBaseException(String id, Object[] args) {
		log.info("***SETTING THE MESSAGE CONTEXT****");
		log.info("Default Locale is " + Locale.getDefault());
		log.info("Message source being loaded is***" + MessageSourceBundle.messageSource());
		System.out.println(id);
		System.out.println(args);
		this.message = MessageSourceBundle.messageSource().getMessage(id, args, "Exception Condition",
				Locale.getDefault());
		this.id = id;
		log.info("****THE MESSAGE THAT IS BEING SET IS****" + message);
	}

	/**
	 * Constructs a new BaseException with the specified cause, Object arguments and
	 * a detail message of (cause==null ? null : cause.toString()) (which typically
	 * contains the class and detail message of cause).
	 */
	public SyzBaseException(Throwable cause, Object[] args) {
		super(cause);
		this.args = args;
	}

	/**
	 * Constructs a new BaseException with the specified message id, Object
	 * arguments and cause.
	 */
	public SyzBaseException(String id, Throwable cause, Object[] args) {
		super(id, cause);
		this.id = id;
		this.args = args;
	}

	/**
	 * Constructs a new BaseException based on the specified message id and list of
	 * object arguments
	 */
	public SyzBaseException(String[] ids, List<Object[]> args) {
		int iIndex = 0;
		for (String str : ids) {
			sbldErrorMessage.append(MessageSourceBundle.messageSource().getMessage(str, args.get(iIndex),
					"Exceptional Situation", Locale.getDefault()));
			sbldErrorMessage.append("\n");
			iIndex++;
		}
		this.message = sbldErrorMessage.toString();
		log.debug("The final exception string is " + sbldErrorMessage.toString());
	}

	/**
	 * Constructs a new BaseException based on the specified list of message codes
	 */
	public SyzBaseException(String[] ids) {
		for (String str : ids) {
			sbldErrorMessage.append(MessageSourceBundle.messageSource().getMessage(str, null, "Exceptional Situation",
					Locale.getDefault()));
			sbldErrorMessage.append("|*|");
		}
		this.message = sbldErrorMessage.toString();
		this.ids = ids;
		log.info("The final exception string is " + sbldErrorMessage.toString());
	}

	/* Fetching message for a given exception code */
	public String getMessage() {
		log.info("going to call the message handler****" + this.message);
		return this.message;
	}

	/**
	 * @return String id
	 */
	public String getId() {
		return this.id;
	}

	public String[] getIds() {
		return this.ids;
	}

	public Errors getErrors() {
		return errors;
	}
}
