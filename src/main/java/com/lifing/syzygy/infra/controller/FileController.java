package com.lifing.syzygy.infra.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpServletRequest;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.util.IOUtils;
import com.lifing.syzygy.infra.exception.ErrorResponse;
import com.lifing.syzygy.infra.util.SessionScope;

import net.coobird.thumbnailator.Thumbnails;

//@CrossOrigin("*")
@RestController
public class FileController {

	@Autowired
	private Environment env;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = { "${api.file.upload}", "${api.file.upload}" + "{/page}" }, method = { RequestMethod.POST,
			RequestMethod.GET })
	public @ResponseBody Object handleFileUpload(@PathVariable(required = false, name = "page") String page,
			@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		log.warn("##### File being uploaded = {}", file);
		log.warn("##### URL", page);

		Document authProfile = (Document) (SessionScope.currentUser.get(request.getHeader("X-Access-Token")));
		// get the profileId to add it to filename.
		String profileId;
		try {
			profileId = authProfile.getString("profileId") != null ? authProfile.getString("profileId")
					: authProfile.getString("businessId");
		} catch (Exception e2) {
			profileId = "";
		}

		MultipartFile awsFile = file;

		if (!file.isEmpty()) {

			try {
				SuccessResponse response = new SuccessResponse();
				response.setDisplayName(file.getOriginalFilename());
				String orgName = String.valueOf(System.currentTimeMillis()) + profileId + file.getOriginalFilename();
				System.out.format("Uploading filename %s: ", orgName);

				// Get AWS metadata from env
				try {
					String clientRegion = env.getProperty("file.upload.region");
					String bucketName = env.getProperty("file.upload.bucketName");
					log.info("bucketname: " + bucketName);
					String keyName = orgName;
					String keyNameOriginal = "original_" + orgName;

					System.out.format("Uploading %s to S3 bucket %s...\n", file, bucketName);

					// Get AWS credentials from env
					BasicAWSCredentials credentials = new BasicAWSCredentials(env.getProperty("AWS_ACCESS_KEY_ID"),
							env.getProperty("AWS_SECRET_ACCESS_KEY"));

					// Build AWS client from the metadata and credentials
					AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
							.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(clientRegion)
							.build();
					InputStream is = awsFile.getInputStream();
					TransferManager tm = TransferManagerBuilder.standard().withS3Client(s3Client).build();
					// do this only for images
					if (awsFile.getContentType().contains("image")) {
						try {

							BufferedImage bi = ImageIO.read(is);
							// Upload the original unedited version of image
							try {
								log.info("uploading original unedited image");
								Upload upload = tm.upload(bucketName, keyNameOriginal, convertMultipartToFile(awsFile));
								upload.waitForCompletion();
							} catch (AmazonServiceException e) {
								// The call was transmitted successfully, but Amazon S3 couldn't process
								// it, so it returned an error response.
								e.printStackTrace();
							} catch (SdkClientException e) {
								// Amazon S3 couldn't be contacted for a response, or the client
								// couldn't parse the response from Amazon S3.
								e.printStackTrace();
							}

							// in KBs
							long fileSize = awsFile.getSize() / 1000;
							// Compress Images ONLY if they are more than 150 KBs in size
							log.info("filesize: " + fileSize + " KB");

							// DONT compress
							if (fileSize <= 2000) {
								try {
									log.info("uploading uncompressed image");
									tm.upload(bucketName, keyName, convertMultipartToFile(awsFile));

								} catch (AmazonServiceException e) {
									// The call was transmitted successfully, but Amazon S3 couldn't process
									// it, so it returned an error response.
									e.printStackTrace();
								} catch (SdkClientException e) {
									// Amazon S3 couldn't be contacted for a response, or the client
									// couldn't parse the response from Amazon S3.
									e.printStackTrace();
								}
							}
							// Compress the image before storing to S3
							else {

								File compressedFile = compress(bi, awsFile.getOriginalFilename());

								log.info("filesize after compression: " + getFileSizeKiloBytes(compressedFile) + " KB");
								try {
									log.info("uploading compressed image");
									tm.upload(bucketName, keyName, compressedFile);
								} catch (AmazonServiceException e) {
									// The call was transmitted successfully, but Amazon S3 couldn't process
									// it, so it returned an error response.
									e.printStackTrace();
								} catch (SdkClientException e) {
									// Amazon S3 couldn't be contacted for a response, or the client
									// couldn't parse the response from Amazon S3.
									e.printStackTrace();
								}
							}
							
							//set height and width
							response.setHeight(bi.getHeight());
							response.setWidth(bi.getWidth());

						}

						catch (Exception e1) {
							// TODO Auto-generated catch block

							e1.printStackTrace();
							log.error("compression error", e1);
						}
						response.setOriginalFilePath(
								((AmazonS3Client) s3Client).getResourceUrl(bucketName, keyNameOriginal));
					}
					// Non-image files
					else {

						try {
							log.info("uploading non-image file");

							// This is done to avoid the "content-length not set" error
							byte[] bytes = IOUtils.toByteArray(is);
							ObjectMetadata metaData = new ObjectMetadata();
							metaData.setContentLength(bytes.length);
							ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

							// Upload the file to S3 bucket
							try {
								s3Client.putObject(
										new PutObjectRequest(bucketName, keyName, byteArrayInputStream, metaData));
							} catch (AmazonServiceException e) {
								e.printStackTrace();
							}
						} catch (AmazonServiceException e) {
							// The call was transmitted successfully, but Amazon S3 couldn't process
							// it, so it returned an error response.
							e.printStackTrace();
						} catch (SdkClientException e) {
							// Amazon S3 couldn't be contacted for a response, or the client
							// couldn't parse the response from Amazon S3.
							e.printStackTrace();
						}

					}
					response.setFilePath(((AmazonS3Client) s3Client).getResourceUrl(bucketName, keyName));

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.error("S3 issue", e);
				}

				String uploadsDir = env.getProperty("file.upload.root");
				if (!new File(uploadsDir).exists()) {
					new File(uploadsDir).mkdir();
				}

				log.info("realPathtoUploads = {}", uploadsDir);

				String uploadDirPath = env.getProperty("file.upload.path");

				if (page != null && page.equals("page")) {
					uploadDirPath = uploadDirPath + "page/";
					if (!new File(uploadsDir + uploadDirPath).exists()) {
						new File(uploadsDir + uploadDirPath).mkdir();
					}
				}

				String filePath = uploadsDir + uploadDirPath + orgName;
				log.info("Target file path being saved #### " + filePath);

				// Convert the original "Multipart" file to "File"
				// And
				// Create the thumbnail from it
				try {
					// do this only for images
					if (file.getContentType().contains("image")) {
						File originalMultipartFile = new File(filePath);
						file.transferTo(originalMultipartFile);
						File thumbnailFile = new File(filePath);
						Thumbnails.of(originalMultipartFile).size(300, 300).toFile(thumbnailFile);

						// Append the Base Url to the thumbnail path
						String split[]  = thumbnailFile.getName().split("/");
						response.setThumbnailPath(env.getProperty("api.baseurl") + uploadDirPath + split[split.length-1]);
						//response.setThumbnailPath(env.getProperty("api.baseurl") + uploadDirPath + thumbnailFile);
					} else if (file.getContentType().contains("video")) {
						String thumbFile = VideoThumbnail.generate(response.getFilePath(), uploadsDir + uploadDirPath, response);
						String split[]  = thumbFile.split("/");
						response.setThumbnailPath(env.getProperty("api.baseurl") + uploadDirPath + split[split.length-1]);
					}

				} catch (Exception e1) {
					// TODO Auto-generated catch block

					e1.printStackTrace();
					log.error("thumb transfer error", e1);
				}

				response.setMessage("success");

				response.setFileName(orgName);
				return ResponseEntity.status(HttpStatus.OK).body(response);
			} catch (Exception e) {
				e.printStackTrace();
				ErrorResponse response = new ErrorResponse();
				response.setErrorMessage("File upload failed");
				response.setStackTrace(e.getMessage());
				log.error("handleFileUpload error", e);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		} else {
			ErrorResponse response = new ErrorResponse();
			response.setErrorMessage("File is empty");
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(response);
		}
	}

	private static String getFileSizeKiloBytes(File file) {
		return (double) file.length() / 1024 + "  kb";
	}

	private static File convertMultipartToFile(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos;
		try {
			convFile.createNewFile();
			fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return convFile;
	}

	private static File compress(BufferedImage bi, String fileName) throws FileNotFoundException, IOException {
		float compressionFactor = 0.5f;
		int compression = 1;
		Iterator<ImageWriter> i = ImageIO.getImageWritersByFormatName("jpeg");
		ImageWriter jpegWriter = i.next();
		// Set the compression quality
		ImageWriteParam param = jpegWriter.getDefaultWriteParam();
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(compressionFactor * compression);

		File file = new File(compressionFactor + "_" + fileName);

		// Write the image to a file
		FileImageOutputStream out = new FileImageOutputStream(file);
		jpegWriter.setOutput(out);
		jpegWriter.write(null, new IIOImage(bi, null, null), param);
		jpegWriter.dispose();
		out.close();
		return file;
	}

}
