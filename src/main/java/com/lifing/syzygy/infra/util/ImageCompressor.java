package com.lifing.syzygy.infra.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.springframework.stereotype.Component;

@Component
public class ImageCompressor {

	public void compressImage(File imageFile) {

		try {
			if (imageFile.canWrite()) {
				String[] names = imageFile.getName().split("\\.");
				String extension = names[names.length - 1];

				// String[] imageFiles = imageFile.getName().split("\\/");
				// imageFile = new File(imageFiles[imageFiles.length - 1]);

				File compressedImageFile = new File(imageFile.getPath());

				InputStream is = new FileInputStream(compressedImageFile);
				OutputStream os = new FileOutputStream(compressedImageFile);

				float quality = 0.5f;

				// create a BufferedImage as the result of decoding the supplied InputStream

				BufferedImage image = ImageIO.read(is);

				// get all image writers for JPG format
				ImageWriter writer = null;
				ImageWriteParam param = null;
				if (extension.equals("png")) {
					writer = ImageIO.getImageWritersByFormatName("png").next();
					param = writer.getDefaultWriteParam();

					// compress to a given quality
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				} else if (extension.equals("jpg")) {
					writer = ImageIO.getImageWritersByFormatName("jpg").next();
					// compress to a given quality
					param = writer.getDefaultWriteParam();
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				} else if (extension.equals("jpeg")) {
					writer = ImageIO.getImageWritersByFormatName("jpeg").next();
					// compress to a given quality
					param = writer.getDefaultWriteParam();
					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
				}

				// ImageIO.getImageWritersByFormatName("jpg");

				ImageOutputStream ios = ImageIO.createImageOutputStream(os);
				writer.setOutput(ios);

				param.setCompressionQuality(quality);

				// appends a complete image stream containing a single image and
				// associated stream and image metadata and thumbnails to the output
				writer.write(null, new IIOImage(image, null, null), param);

				// close all streams
				is.close();
				os.close();
				ios.close();
				writer.dispose();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
