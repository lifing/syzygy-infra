# Syzygy Infrastructure
This repository is an infrastructure module of the syzygy platform 

[ ![Codeship Status for lifing/syzygy-infra](https://app.codeship.com/projects/d71b7610-3fba-0135-0f8f-42b185f46caf/status?branch=master)](https://app.codeship.com/projects/229853)

# Development
### Pre-requisites

Install oracle-java8-installer 

Install eclipse-neon


### Set up project

fork syzygy-infra into your personal repository

clone the forked repository (either ssh or https) into your system using terminal window

```
git clone https://<your_bitbuket_id>@bitbucket.org/lifing/syzygy-infra.git

```

# Build project

We are using Gradle(instead of maven) to build an individual service

Run the following to build a project
 
```
clean assemble
```

# Running the App
This module will be used as a library for syzygy services, it will be shipped with every Java service as a dependency